// One context per glCanvas
function MainGLContext(vertShaderAddr, fragShaderAddr, gameOverAddr)
{
	var gl;
	var shaderProgram;
	var vertexPositionAttribute;
	var quadBuffer;
	var texField = -1;
	var texTile;
	var texRad = -1;
	var samplerLoc;
	var samplerTileLoc;
	var samplerRadLoc;
	var fParamsLoc;
  var alphaLoc;
	var vertexElem = vertShaderAddr
	var fragmentElem = fragShaderAddr
	var fragmentGameOverElem = gameOverAddr
	// GameOver features
	var shaderProgram2;
	var samplerGameOverLoc;
	var texGameOver;
	var texHelp;
	
	this.initTileTexture = function(tileImage)
	{
		texTile = gl.createTexture();
	  
	  gl.bindTexture(gl.TEXTURE_2D, texTile);
	  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tileImage);
	  
	  gl.bindTexture(gl.TEXTURE_2D, null);
	}

	this.initGameOverTexture = function(goImg)
	{
	  texGameOver = gl.createTexture();
	  
	  gl.bindTexture(gl.TEXTURE_2D, texGameOver);
	  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, goImg);
	  
	  gl.bindTexture(gl.TEXTURE_2D, null);
	}

	this.initHelpTexture = function(hImg)
	{
	  texHelp = gl.createTexture();
	  
	  gl.bindTexture(gl.TEXTURE_2D, texHelp);
	  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, hImg);
	  
	  gl.bindTexture(gl.TEXTURE_2D, null);
	}

	this.updateFieldGPU = function(arrField)
	{
		gl.bindTexture(gl.TEXTURE_2D, texField);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 16, 16, 0, gl.RGBA, gl.UNSIGNED_BYTE, arrField);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	this.updateRadGPU = function(arrRad)
	{
	  gl.bindTexture(gl.TEXTURE_2D, texRad);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.ALPHA, 16, 16, 0, gl.ALPHA, gl.UNSIGNED_BYTE, arrRad);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	this.drawScreenTex = function(params) {
	  gl.useProgram(shaderProgram2);
	  vertexPositionAttribute = gl.getAttribLocation(shaderProgram2, "aVertexPosition");
	  gl.enableVertexAttribArray(vertexPositionAttribute);
	  
	  gl.bindBuffer(gl.ARRAY_BUFFER, quadBuffer);
	  gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);
	  
	  gl.activeTexture(gl.TEXTURE0);
	  if (params.id == 0) {
		gl.bindTexture(gl.TEXTURE_2D, texGameOver);
	  } else {
		gl.bindTexture(gl.TEXTURE_2D, texHelp);
	  }
	  
	  gl.uniform1i(samplerGameOverLoc,     0);
	  gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	}

	this.switchGPUProgramToDefault = function()
	{
	  gl.useProgram(shaderProgram);
	}

	this.drawScene = function() {  
	  gl.bindBuffer(gl.ARRAY_BUFFER, quadBuffer);
	  gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);
	  
	  gl.activeTexture(gl.TEXTURE0);
	  gl.bindTexture(gl.TEXTURE_2D, texField);
	  gl.activeTexture(gl.TEXTURE1);
	  gl.bindTexture(gl.TEXTURE_2D, texTile);
	  gl.activeTexture(gl.TEXTURE2);
	  gl.bindTexture(gl.TEXTURE_2D, texRad);
	  
	  gl.uniform1i(samplerLoc,     0);
	  gl.uniform1i(samplerTileLoc, 1);  
	  gl.uniform1i(samplerRadLoc,  2);
	  gl.uniform4f(fParamsLoc, 9.0 / 16.0, 9.0 / 16.0, 16.0, 1.0 / 9.0);
    gl.uniform1f(alphaLoc, 1.0);
	  gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	}

	function getGLContext(canvas)
	{
		try {
			gl = canvas.getContext("webgl", {depth : false, stencil : false, antialias : false, premultipliedAlpha : false});
			if (!gl) {
				gl = canvas.getContext("experimental-webgl", {depth : false, stencil : false, antialias : false, premultipliedAlpha : false});
			}
		}
		catch(e) {}
		
		if (!gl) {
			alert("Could not get WebGL context! =(");
		}
		gl.clearColor(1.0, 0.0, 0.0, 1.0);
		gl.disable(gl.DEPTH_TEST);
		gl.disable(gl.CULL_FACE);
		return gl;
	}

	this.init = function(canvas)
	{
	  getGLContext(canvas);
	  initShaders();
	  initBuffers();
	  initOthers();
	}

	function getShader(id)
	{
	  var shaderScript, theSource, currentChild, shader;
	   
	  shaderScript = document.getElementById(id);
	   
	  if (!shaderScript) {
		return null;
	  }
	   
	  theSource = "";
	  currentChild = shaderScript.firstChild;
	   
	  while(currentChild) {
		if (currentChild.nodeType == currentChild.TEXT_NODE) {
		  theSource += currentChild.textContent;
		}
		 
		currentChild = currentChild.nextSibling;
	  }
	  
	  if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	  } else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	  } else {
		 // Unknown shader type
		 return null;
	  }
	  gl.shaderSource(shader, theSource);
		 
	  // Compile the shader program
	  gl.compileShader(shader);  
		 
	  // See if it compiled successfully
	  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {  
		  alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));  
		  return null;  
	  }
		 
	  return shader;
	}

	function initShaders() {
	  var fragmentShader = getShader(fragmentElem);
	  var vertexShader = getShader(vertexElem);
	  var gameOverShader = getShader(fragmentGameOverElem);
	   
	  // Create the shader program
	   
	  shaderProgram = gl.createProgram();
	  gl.attachShader(shaderProgram, vertexShader);
	  gl.attachShader(shaderProgram, fragmentShader);
	  gl.linkProgram(shaderProgram);
	  // GameOver features 
	  shaderProgram2 = gl.createProgram();
	  gl.attachShader(shaderProgram2, vertexShader);
	  gl.attachShader(shaderProgram2, gameOverShader);
	  gl.linkProgram(shaderProgram2);
	  // If creating the shader program failed, alert
	   
	  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Unable to initialize the shader program.");
	  }
	  if (!gl.getProgramParameter(shaderProgram2, gl.LINK_STATUS)) {
		alert("Unable to initialize the shader program2.");
	  }
	  gl.useProgram(shaderProgram);
	   
	  vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
	  gl.enableVertexAttribArray(vertexPositionAttribute);
	}

	function initBuffers() {
	  quadBuffer = gl.createBuffer();
	  gl.bindBuffer(gl.ARRAY_BUFFER, quadBuffer);
	   
	  var vertices = [
		1.0,  1.0,  0.0,
		-1.0, 1.0,  0.0,
		1.0,  -1.0, 0.0,
		-1.0, -1.0, 0.0
	  ];
	   
	  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	}

	function initOthers() {
	  // Init texture objects
	  if (texField == -1)
		texField = gl.createTexture();
	  if (texRad == -1)
		texRad = gl.createTexture();
	  
	  samplerLoc = gl.getUniformLocation(shaderProgram, "fieldTex");
	  samplerTileLoc = gl.getUniformLocation(shaderProgram, "tileTex");
	  samplerRadLoc = gl.getUniformLocation(shaderProgram, "radTex");
	  fParamsLoc = gl.getUniformLocation(shaderProgram, "fieldParams");
    alphaLoc = gl.getUniformLocation(shaderProgram, "alphaFactor");
	  samplerGameOverLoc = gl.getUniformLocation(shaderProgram2, "gameOverTex");
	}
}
// Utility GL context for drawing next-turn balls
function UtilGLContext(vertShaderAddr, fragShaderAddr)
{
	var gl;
	var shaderProgram;
	var vertexPositionAttribute;
	var quadBuffer;
	var texField = -1;
	var texTile;
	var texRad = -1;
	var samplerLoc;
	var samplerTileLoc;
	var samplerRadLoc;
	var fParamsLoc;
  var alphaLoc;
	var vertexElem = vertShaderAddr
	var fragmentElem = fragShaderAddr

	
	this.initTileTexture = function(tileImage)
	{
		texTile = gl.createTexture();
	  
	  gl.bindTexture(gl.TEXTURE_2D, texTile);
	  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tileImage);
	  
	  gl.bindTexture(gl.TEXTURE_2D, null);
	}

	this.updateFieldGPU = function(arrField)
	{
		gl.bindTexture(gl.TEXTURE_2D, texField);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 4, 4, 0, gl.RGBA, gl.UNSIGNED_BYTE, arrField);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	this.updateRadGPU = function(arrRad)
	{
	  gl.bindTexture(gl.TEXTURE_2D, texRad);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.ALPHA, 4, 4, 0, gl.ALPHA, gl.UNSIGNED_BYTE, arrRad);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}


	this.drawScene = function() {  
	  gl.bindBuffer(gl.ARRAY_BUFFER, quadBuffer);
	  gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);
	  
	  gl.activeTexture(gl.TEXTURE0);
	  gl.bindTexture(gl.TEXTURE_2D, texField);
	  gl.activeTexture(gl.TEXTURE1);
	  gl.bindTexture(gl.TEXTURE_2D, texTile);
	  gl.activeTexture(gl.TEXTURE2);
	  gl.bindTexture(gl.TEXTURE_2D, texRad);
	  
	  gl.uniform1i(samplerLoc,     0);
	  gl.uniform1i(samplerTileLoc, 1);  
	  gl.uniform1i(samplerRadLoc,  2);
	  gl.uniform4f(fParamsLoc, 3.0 / 4.0, 1.0 / 4.0, 4.0, 1.0 / 16.0);
    gl.uniform1f(alphaLoc, 0.0);
	  gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	}
	
	
	
	function getGLContext(canvas)
	{
		try {
			gl = canvas.getContext("webgl", {depth : false, stencil : false, antialias : false, premultipliedAlpha : false});
			if (!gl) {
				gl = canvas.getContext("experimental-webgl", {depth : false, stencil : false, antialias : false, premultipliedAlpha : false});
			}
		}
		catch(e) {}
		
		if (!gl) {
			alert("Could not get WebGL context! =(");
		}
		gl.clearColor(1.0, 0.0, 0.0, 1.0);
		gl.disable(gl.DEPTH_TEST);
		gl.disable(gl.CULL_FACE);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    gl.enable(gl.BLEND);
		return gl;
	}

	this.init = function(canvas)
	{
	  getGLContext(canvas);
	  initShaders();
	  initBuffers();
	  initOthers();
	}

	function getShader(id)
	{
	  var shaderScript, theSource, currentChild, shader;
	   
	  shaderScript = document.getElementById(id);
	   
	  if (!shaderScript) {
		return null;
	  }
	   
	  theSource = "";
	  currentChild = shaderScript.firstChild;
	   
	  while(currentChild) {
		if (currentChild.nodeType == currentChild.TEXT_NODE) {
		  theSource += currentChild.textContent;
		}
		 
		currentChild = currentChild.nextSibling;
	  }
	  
	  if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	  } else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	  } else {
		 // Unknown shader type
		 return null;
	  }
	  gl.shaderSource(shader, theSource);
		 
	  // Compile the shader program
	  gl.compileShader(shader);  
		 
	  // See if it compiled successfully
	  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {  
		  alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));  
		  return null;  
	  }
		 
	  return shader;
	}

	function initShaders() {
	  var fragmentShader = getShader(fragmentElem);
	  var vertexShader = getShader(vertexElem);
	   
	  // Create the shader program
	   
	  shaderProgram = gl.createProgram();
	  gl.attachShader(shaderProgram, vertexShader);
	  gl.attachShader(shaderProgram, fragmentShader);
	  gl.linkProgram(shaderProgram);
	  
	  // If creating the shader program failed, alert
	   
	  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Unable to initialize the shader program.");
	  }
	  gl.useProgram(shaderProgram);
	   
	  vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
	  gl.enableVertexAttribArray(vertexPositionAttribute);
	}

	function initBuffers() {
	  quadBuffer = gl.createBuffer();
	  gl.bindBuffer(gl.ARRAY_BUFFER, quadBuffer);
	   
	  var vertices = [
		1.0,  1.0,  0.0,
		-1.0, 1.0,  0.0,
		1.0,  -1.0, 0.0,
		-1.0, -1.0, 0.0
	  ];
	   
	  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	}

	function initOthers() {
	  // Init texture objects
	  if (texField == -1)
		texField = gl.createTexture();
	  if (texRad == -1)
		texRad = gl.createTexture();
	  
	  samplerLoc = gl.getUniformLocation(shaderProgram, "fieldTex");
	  samplerTileLoc = gl.getUniformLocation(shaderProgram, "tileTex");
	  samplerRadLoc = gl.getUniformLocation(shaderProgram, "radTex");
	  fParamsLoc = gl.getUniformLocation(shaderProgram, "fieldParams");
    alphaLoc = gl.getUniformLocation(shaderProgram, "alphaFactor");
	}
}



